//
//  main.cpp
//  Transport
//
//  Created by Juan Albarran on 10/22/14.
//  Copyright (c) 2014 OUTLIER. All rights reserved.
//

#include <iostream>
#include "Vehiculo.h"
#include "Empleado.h"

using namespace std;

int main(int argc, const char *argv[]) {
    Vehiculo *bus1 = new Vehiculo("ASDF1", 30, 3000, NORMAL);
    bus1->toString();
    Empleado *conductor = new Empleado(CONDUCTOR, "Juan", "123123-4", 123123);
    conductor->toString();
    return 0;
}