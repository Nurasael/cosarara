//
// Created by Juan Albarran on 10/22/14.
// Copyright (c) 2014 OUTLIER. All rights reserved.
//


#ifndef __Travel_H_
#define __Travel_H_

#include "Vehiculo.h"
#include "Empleado.h"
#include "Pasajero.h"
#include "Horario/Horario.h"
#include "Ciudad/Ciudad.h"
#include <vector>

class Viaje {

private:
    Vehiculo vehiculo;
    Ciudad ciudades[2];
    Horario horarios[2];
    Empleado chofer;
    Empleado auxiliar;
    std::vector<Pasajero> pasajeros;
};


#endif //__Travel_H_
