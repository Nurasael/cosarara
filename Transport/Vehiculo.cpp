//
// Created by Juan Albarran on 10/22/14.
// Copyright (c) 2014 OUTLIER. All rights reserved.
//

#include "Vehiculo.h"

Vehiculo::Vehiculo(std::string const &matricula, int capacidad, int costo, TipoVehiculo const &tipo)
        : matricula(matricula), capacidad(capacidad), costo(costo), tipo(tipo) {
}

int Vehiculo::getCapacidad() const {
    return capacidad;
}

int Vehiculo::getCosto() const {
    return costo;
}

TipoVehiculo const &Vehiculo::getTipo() const {
    return tipo;
}


void Vehiculo::toString() {
    printf("Vehiculo(matricula: %s, capacidad: %d, costo: %d, tipo: %d)\n", matricula.c_str(), capacidad, costo, tipo);
}

std::string const &Vehiculo::getMatricula() const {
    return matricula;
}