//
// Created by Juan Albarran on 10/22/14.
// Copyright (c) 2014 OUTLIER. All rights reserved.
//

#include <iosfwd>
#include <string>

#ifndef __Person_H_
#define __Person_H_

class Persona {

protected:
    std::string nombre;
    std::string rut;
    int telefono;
public:
    int getTelefono() const;

    std::string const &getNombre() const;

    std::string const &getRut() const;

    virtual void toString() = 0;
};


#endif //__Person_H_
