//
// Created by Juan Albarran on 10/22/14.
// Copyright (c) 2014 OUTLIER. All rights reserved.
//


#import "Persona.h"

#ifndef __Employee_H_
#define __Employee_H_

enum TipoEmpleado {
    CONDUCTOR,
    ASISTENTE,
    ADMIN
};

class Empleado : Persona {

public:

    Empleado(TipoEmpleado const &tipo, std::string const &nombre, std::string const &rut, int telefono);

    virtual void toString();

public:
    TipoEmpleado const &getTipo() const;

    int getSueldo() const;

    void setSueldo(int sueldo);

private:
    TipoEmpleado tipo;
    int sueldo;
};


#endif //__Employee_H_
