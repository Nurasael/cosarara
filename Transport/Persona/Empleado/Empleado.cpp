//
// Created by Juan Albarran on 10/22/14.
// Copyright (c) 2014 OUTLIER. All rights reserved.
//

#include "Empleado.h"

void Empleado::toString() {
    printf("Empleado(nombre: %s, rut: %s, telefono: %d, tipo: %d, sueldo: %d)\n", nombre.c_str(), rut.c_str(), telefono, tipo, sueldo);
}

Empleado::Empleado(TipoEmpleado const &tipo, std::string const &nombre, std::string const &rut, int telefono)
        : tipo(tipo) {
    switch (tipo) {
        case CONDUCTOR:
            sueldo = 20000;
            break;
        case ASISTENTE:
            sueldo = 10000;
            break;
        case ADMIN:
            sueldo = 30000;
            break;
    }
    this->nombre = nombre;
    this->rut = rut;
    this->telefono = telefono;
}

TipoEmpleado const &Empleado::getTipo() const {
    return tipo;
}

int Empleado::getSueldo() const {
    return sueldo;
}

void Empleado::setSueldo(int sueldo) {
    Empleado::sueldo = sueldo;
}