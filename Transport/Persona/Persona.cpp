//
// Created by Juan Albarran on 10/22/14.
// Copyright (c) 2014 OUTLIER. All rights reserved.
//

#include "Persona.h"

int Persona::getTelefono() const {
    return telefono;
}

std::string const &Persona::getRut() const {
    return rut;
}

std::string const &Persona::getNombre() const {
    return nombre;
}