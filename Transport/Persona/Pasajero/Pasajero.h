//
// Created by Juan Albarran on 10/22/14.
// Copyright (c) 2014 OUTLIER. All rights reserved.
//

#ifndef __Passenger_H_
#define __Passenger_H_

#include "Persona.h"
#include "Vehiculo.h"

class Pasajero : Persona {

public:
    Pasajero(int asiento, int monto, Vehiculo const &bus, std::string const &nombre, std::string const &rut, int telefono);

    int getAsiento() const;

    Vehiculo const &getBus() const;

    int getMonto() const;

    virtual void toString();

private:
    int asiento;
    Vehiculo bus;
    int monto;
};


#endif //__Passenger_H_
