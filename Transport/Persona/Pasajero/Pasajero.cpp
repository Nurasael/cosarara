//
// Created by Juan Albarran on 10/22/14.
// Copyright (c) 2014 OUTLIER. All rights reserved.
//

#include "Pasajero.h"

Pasajero::Pasajero(int asiento, int monto, Vehiculo const &bus, std::string const &nombre, std::string const &rut, int telefono)
        : asiento(asiento), monto(monto), bus(bus) {
    this->nombre = nombre;
    this->rut = rut;
    this->telefono = telefono;
}

int Pasajero::getAsiento() const {
    return asiento;
}

Vehiculo const &Pasajero::getBus() const {
    return bus;
}

int Pasajero::getMonto() const {
    return monto;
}

void Pasajero::toString() {
    printf("Empleado(nombre: %s, rut: %s, telefono: %d, asiento: %d, monto: %d", nombre.c_str(), rut.c_str(), telefono, asiento, monto);
    bus.toString();
}
