//
// Created by Juan Albarran on 10/22/14.
// Copyright (c) 2014 OUTLIER. All rights reserved.
//


#ifndef __Vehicle_H_
#define __Vehicle_H_

#include <iostream>

enum TipoVehiculo {
    NORMAL,
    DOBLE,
    DELUJO,
    FURGON
};

class Vehiculo {

public:
    Vehiculo(std::string const &matricula, int capacidad, int costo, TipoVehiculo const &tipo);

    std::string const &getMatricula() const;

    int getCapacidad() const;

    int getCosto() const;

    TipoVehiculo const &getTipo() const;

    void toString();

private:
    std::string matricula;
    int capacidad;
    int costo;
    TipoVehiculo tipo;
};


#endif //__Vehicle_H_
